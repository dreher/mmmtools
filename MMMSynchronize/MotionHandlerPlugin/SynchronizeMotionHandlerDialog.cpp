#include "SynchronizeMotionHandlerDialog.h"
#include "ui_SynchronizeMotionHandlerDialog.h"

#include <QCheckBox>
#include <set>

SynchronizeMotionHandlerDialog::SynchronizeMotionHandlerDialog(QWidget* parent, MMM::MotionList motions) :
    QDialog(parent),
    ui(new Ui::SynchronizeMotionHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    allMotion(true),
    synchronized(false)
{
    ui->setupUi(this);

    loadMotions();

    connect(ui->AllMotionCheckBox, SIGNAL(toggled(bool)), this, SLOT(allMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), this, SLOT(chooseMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), ui->ChooseMotionComboBox, SLOT(setEnabled(bool)));
    connect(ui->ChooseMotionComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setCurrentMotion(int)));
    connect(ui->SynchronizeButton, SIGNAL(clicked()), this, SLOT(synchronize()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

SynchronizeMotionHandlerDialog::~SynchronizeMotionHandlerDialog() {
    delete ui;
}

bool SynchronizeMotionHandlerDialog::synchronizeMotion() {
    exec();
    return synchronized;
}

void SynchronizeMotionHandlerDialog::loadMotions() {
    for (MMM::MotionPtr motion : motions) {
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(motion->getName()));
    }
    setCurrentMotion(0);
}

void SynchronizeMotionHandlerDialog::setCurrentMotion(int index) {
    if (index > 0 && motions.size() > static_cast<std::size_t>(index)) {
        currentMotion = motions[index];
    }
}

void SynchronizeMotionHandlerDialog::chooseMotionToggled(bool checked) {
    ui->AllMotionCheckBox->setChecked(!checked);
    allMotion = !checked;
}

void SynchronizeMotionHandlerDialog::allMotionToggled(bool checked) {
    ui->ChooseMotionGroupBox->setChecked(!checked);
    allMotion = checked;
}

void SynchronizeMotionHandlerDialog::synchronize() {
    float timeFrequency = 1.0f / ui->FPSSpin->value();
    if (allMotion) {
        for (MMM::MotionPtr motion : motions) {
            motion->synchronizeSensorMeasurements(timeFrequency);
        }
    } else {
        currentMotion->synchronizeSensorMeasurements(timeFrequency);
    }
    synchronized = true;
    this->close();
}
