#include "AddDataGloveHandlerFactory.h"
#include "AddDataGloveHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddDataGloveHandlerFactory::registry(AddDataGloveHandler::NAME, &AddDataGloveHandlerFactory::createInstance);

AddDataGloveHandlerFactory::AddDataGloveHandlerFactory() : MotionHandlerFactory() {}

AddDataGloveHandlerFactory::~AddDataGloveHandlerFactory() {}

std::string AddDataGloveHandlerFactory::getName() {
    return AddDataGloveHandler::NAME;
}

MotionHandlerPtr AddDataGloveHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new AddDataGloveHandler(widget));
}

MotionHandlerFactoryPtr AddDataGloveHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddDataGloveHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddDataGloveHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
