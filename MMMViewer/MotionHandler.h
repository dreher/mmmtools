/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONHANDLER_H_
#define __MMM_MOTIONHANDLER_H_

#include <boost/shared_ptr.hpp>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "PluginHandler/PluginHandler.h"
#endif

#include <QWidget>

namespace MMM
{

enum MotionHandlerType {
    IMPORT, EXPORT, ADD_SENSOR, GENERAL
};

class MotionHandler : public QObject
{
    Q_OBJECT

public:
    virtual void handleMotion(MotionList motions) = 0;

    virtual std::string getName() = 0;

    virtual MotionHandlerType getType() {
        return type;
    }

    virtual std::string getDescription() {
        return description;
    }

    virtual std::string getShortCut() {
        return shortCut;
    }

    virtual boost::shared_ptr<IPluginHandler> getPluginHandler() {
        return nullptr;
    }

    virtual std::string getPluginHandlerID() {
        return "";
    }

signals:
    void openMotions(MMM::MotionList motions);

    void jumpTo(float timestep);

    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());


protected:
    MotionHandler(const MotionHandlerType &type, const std::string &description, const std::string &shortCut = std::string()) :
        type(type),
        description(description),
        shortCut(shortCut)
    {
    }

private:
    MotionHandlerType type;
    std::string description;
    std::string shortCut;

};

typedef boost::shared_ptr<MotionHandler> MotionHandlerPtr;

}

#endif
