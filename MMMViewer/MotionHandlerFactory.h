/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONHANDLERFACTORY_H_
#define __MMM_MOTIONHANDLERFACTORY_H_

#include "MotionHandler.h"

#include <boost/shared_ptr.hpp>
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/Motion/Sensor.h>
#include <VirtualRobot/RobotConfig.h>
#include <Inventor/nodes/SoSeparator.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <QWidget>

namespace MMM
{

class MotionHandlerFactory : public AbstractFactoryMethod<MotionHandlerFactory, void*>
{
public:
    MotionHandlerFactory() { }

    virtual ~MotionHandlerFactory() { }

    virtual MotionHandlerPtr createMotionHandler(QWidget* widget) = 0;

    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef boost::shared_ptr<MotionHandlerFactory> MotionHandlerFactoryPtr;

}

#endif
