#include "ImageSequenceExporterFactory.h"
#include "ImageSequenceExporter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry ImageSequenceExporterFactory::registry(ImageSequenceExporter::NAME, &ImageSequenceExporterFactory::createInstance);

ImageSequenceExporterFactory::ImageSequenceExporterFactory() : MotionHandlerFactory() {}

ImageSequenceExporterFactory::~ImageSequenceExporterFactory() {}

std::string ImageSequenceExporterFactory::getName() {
    return ImageSequenceExporter::NAME;
}

MotionHandlerPtr ImageSequenceExporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new ImageSequenceExporter(widget));
}

MotionHandlerFactoryPtr ImageSequenceExporterFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new ImageSequenceExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new ImageSequenceExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
