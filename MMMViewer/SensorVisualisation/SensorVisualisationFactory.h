/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORVISUALISATIONFACTORY_H_
#define __MMM_SENSORVISUALISATIONFACTORY_H_

#include "SensorVisualisation.h"

#include <boost/shared_ptr.hpp>
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/Motion/Sensor.h>
#include <VirtualRobot/RobotConfig.h>
#include <Inventor/nodes/SoSeparator.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

namespace MMM
{

class SensorVisualisationFactory : public AbstractFactoryMethod<SensorVisualisationFactory, void*>
{
public:
    SensorVisualisationFactory() { }

    virtual ~SensorVisualisationFactory() { }

    virtual SensorVisualisationPtr createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) = 0;

    virtual std::string getName() = 0;

    static constexpr const char* VISUALISATION = "Visualisation";

    static constexpr const char* VERSION = "1.0";
};

typedef boost::shared_ptr<SensorVisualisationFactory> SensorVisualisationFactoryPtr;

inline std::string VIS_STR(const char* type) {
    std::string buf(type);
    buf.append(SensorVisualisationFactory::VISUALISATION);
    return buf;
}

}

#endif
