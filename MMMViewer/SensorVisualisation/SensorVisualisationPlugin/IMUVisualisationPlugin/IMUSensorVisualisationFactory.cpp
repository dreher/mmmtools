#include "IMUSensorVisualisationFactory.h"
#include "IMUSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/IMUPlugin/IMUSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry IMUSensorVisualisationFactory::registry(VIS_STR(IMUSensor::TYPE), &IMUSensorVisualisationFactory::createInstance);

IMUSensorVisualisationFactory::IMUSensorVisualisationFactory() : SensorVisualisationFactory() {}

IMUSensorVisualisationFactory::~IMUSensorVisualisationFactory() {}

std::string IMUSensorVisualisationFactory::getName()
{
    return VIS_STR(IMUSensor::TYPE);
}

SensorVisualisationPtr IMUSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    IMUSensorPtr s = boost::dynamic_pointer_cast<IMUSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << IMUSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new IMUSensorVisualisation(s, robot, sceneSep));
}

SensorVisualisationFactoryPtr IMUSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new IMUSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new IMUSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
