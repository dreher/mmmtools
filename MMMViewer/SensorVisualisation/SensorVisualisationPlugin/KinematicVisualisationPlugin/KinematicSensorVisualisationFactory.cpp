#include "KinematicSensorVisualisationFactory.h"
#include "KinematicSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry KinematicSensorVisualisationFactory::registry(VIS_STR(KinematicSensor::TYPE), &KinematicSensorVisualisationFactory::createInstance);

KinematicSensorVisualisationFactory::KinematicSensorVisualisationFactory() : SensorVisualisationFactory() {}

KinematicSensorVisualisationFactory::~KinematicSensorVisualisationFactory() {}

std::string KinematicSensorVisualisationFactory::getName()
{
    return VIS_STR(KinematicSensor::TYPE);
}

SensorVisualisationPtr KinematicSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    KinematicSensorPtr s = boost::dynamic_pointer_cast<KinematicSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << KinematicSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new KinematicSensorVisualisation(s, robot, sceneSep));
}

SensorVisualisationFactoryPtr KinematicSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new KinematicSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new KinematicSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
