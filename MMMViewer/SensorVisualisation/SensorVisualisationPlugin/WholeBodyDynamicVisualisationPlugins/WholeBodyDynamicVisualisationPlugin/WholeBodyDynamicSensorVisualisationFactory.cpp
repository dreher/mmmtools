#include "WholeBodyDynamicSensorVisualisationFactory.h"
#include "WholeBodyDynamicSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/WholeBodyDynamicPlugin/WholeBodyDynamicSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry WholeBodyDynamicSensorVisualisationFactory::registry(VIS_STR(WholeBodyDynamicSensor::TYPE), &WholeBodyDynamicSensorVisualisationFactory::createInstance);

WholeBodyDynamicSensorVisualisationFactory::WholeBodyDynamicSensorVisualisationFactory() : SensorVisualisationFactory() {}

WholeBodyDynamicSensorVisualisationFactory::~WholeBodyDynamicSensorVisualisationFactory() {}

std::string WholeBodyDynamicSensorVisualisationFactory::getName()
{
    return VIS_STR(WholeBodyDynamicSensor::TYPE);
}

SensorVisualisationPtr WholeBodyDynamicSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    WholeBodyDynamicSensorPtr s = boost::dynamic_pointer_cast<WholeBodyDynamicSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << WholeBodyDynamicSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new WholeBodyDynamicSensorVisualisation(s, sceneSep));
}

SensorVisualisationFactoryPtr WholeBodyDynamicSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new WholeBodyDynamicSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new WholeBodyDynamicSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
