#include "MoCapMarkerSensorVisualisationFactory.h"
#include "MoCapMarkerSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/MoCapMarkerPlugin/MoCapMarkerSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry MoCapMarkerSensorVisualisationFactory::registry(VIS_STR(MoCapMarkerSensor::TYPE), &MoCapMarkerSensorVisualisationFactory::createInstance);

MoCapMarkerSensorVisualisationFactory::MoCapMarkerSensorVisualisationFactory() : SensorVisualisationFactory() {}

MoCapMarkerSensorVisualisationFactory::~MoCapMarkerSensorVisualisationFactory() {}

std::string MoCapMarkerSensorVisualisationFactory::getName()
{
    return VIS_STR(MoCapMarkerSensor::TYPE);
}

SensorVisualisationPtr MoCapMarkerSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, boost::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    MoCapMarkerSensorPtr s = boost::dynamic_pointer_cast<MoCapMarkerSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << MoCapMarkerSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new MoCapMarkerSensorVisualisation(s, sceneSep));
}

SensorVisualisationFactoryPtr MoCapMarkerSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new MoCapMarkerSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new MoCapMarkerSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
