#include "CSVConverterFactory.h"

#include <boost/extension/extension.hpp>

#include "CSVConverter.h"

using namespace MMM;

// register this factory
IMUConverterFactory::SubClassRegistry CSVConverterFactory::registry(CSVConverter::TYPE, &CSVConverterFactory::createInstance);

CSVConverterFactory::CSVConverterFactory() : IMUConverterFactory() {}

CSVConverterFactory::~CSVConverterFactory() {}

IMUConverterPtr CSVConverterFactory::createIMUConverter()
{
    return IMUConverterPtr(new CSVConverter());
}

std::string CSVConverterFactory::getName()
{
    return CSVConverter::TYPE;
}

IMUConverterFactoryPtr CSVConverterFactory::createInstance(void *)
{
    return IMUConverterFactoryPtr(new CSVConverterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL IMUConverterFactoryPtr getFactory() {
    return IMUConverterFactoryPtr(new CSVConverterFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return IMUConverterFactory::VERSION;
}

