/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LEGACYMOTIONCONVERTER_H_
#define __MMM_LEGACYMOTIONCONVERTER_H_

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>

#include "../common/MotionListConfiguration.h"

#include <set>

namespace MMM {

class LegacyMotionConverter
{
public:
    LegacyMotionConverter(const std::string &motionFilePath);

    MotionList convertMotions();

    MotionList convertMotions(std::vector<MotionListConfigurationPtr> configurations);

    std::vector<std::string> getMotionNames();

private:
    MotionPtr convertMotion(LegacyMotionPtr legacyMotion);

    LegacyMotionList legacyMotions;
};

typedef boost::shared_ptr<LegacyMotionConverter> LegacyMotionConverterPtr;

}

#endif // __MMM_LEGACYMOTIONCONVERTER_H_
