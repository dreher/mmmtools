/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LEGACYMOTIONIMPORTERDIALOG_H_
#define __MMM_LEGACYMOTIONIMPORTERDIALOG_H_

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <vector>
#include <tuple>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Motion/Motion.h>
#include "../LegacyMotionConverter.h"
#include "../../common/MotionListWidget.h"
#endif

namespace Ui {
class LegacyMotionImporterDialog;
}

class LegacyMotionImporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LegacyMotionImporterDialog(MMM::LegacyMotionConverterPtr converter, QWidget* parent = 0);
    ~LegacyMotionImporterDialog();

    MMM::MotionList getMotions();

private slots:
    void importMotion();

private:
    Ui::LegacyMotionImporterDialog* ui;
    MotionListWidget* motionList;
    MMM::LegacyMotionConverterPtr converter;
    MMM::MotionList motions;
};

#endif // __MMM_LEGACYMOTIONIMPORTERDIALOG_H_
