/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_MOTIONCONVERTERFACTORY_h_
#define _MMM_MOTIONCONVERTERFACTORY_h_


#include "MotionConverter.h"

#include <MMM/MMMCore.h>
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMImportExport.h>

#include <boost/shared_ptr.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>

namespace MMM
{

class MMM_IMPORT_EXPORT MotionConverterFactory  : public AbstractFactoryMethod<MotionConverterFactory, void*>
{
public:

	MotionConverterFactory() {}

	virtual ~MotionConverterFactory() {}

    virtual MotionConverterPtr createMotionConverter(MotionPtr inputMotion, ModelPtr outputModel, ModelPtr outputModelUnprocessed, ModelProcessorPtr outputModelProcessor, const std::string &configFile, const std::string &convertedMotionName = std::string()) = 0;

    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef boost::shared_ptr<MotionConverterFactory> MotionConverterFactoryPtr;


} // namespace MMM

#endif // _MMM_MotionConverterFactory_h_
