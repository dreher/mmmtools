#include "ConvertingMotionHandlerDialog.h"
#include "ui_ConvertingMotionHandlerDialog.h"

#include <MMM/MathTools.h>
#include <boost/lexical_cast.hpp>

static std::exception_ptr teptr = nullptr;
static MMM::MotionPtr convertedMotion = nullptr;

ConvertingMotionHandlerDialog::ConvertingMotionHandlerDialog(QWidget* parent, MMM::MotionConverterPtr motionConverter, MMM::MotionPtr motion) :
    QDialog(parent),
    ui(new Ui::ConvertingMotionHandlerDialog),
    motionConverter(motionConverter),
    convertingThread(&ConvertingMotionHandlerDialog::convert, motionConverter),
    minTimestep(motion->getMinTimestep()),
    maxTimestep(motion->getMaxTimestep()),
    scaling(100),
    updateInterval(1.0f)
{
    teptr = nullptr;
    convertedMotion = nullptr;

    ui->setupUi(this);

    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    ui->ProgressBar->setMinimum(minTimestep * scaling);
    ui->ProgressBar->setMaximum(maxTimestep * scaling);

    updateTimer = new SoTimerSensor(updateTimerCallback, this);
    updateTimer->setInterval(SbTime(updateInterval));
    startingTime = SbTime::getTimeOfDay();
    updateTimer->schedule();

}

ConvertingMotionHandlerDialog::~ConvertingMotionHandlerDialog()
{
    delete ui;
}

void ConvertingMotionHandlerDialog::convert(MMM::MotionConverterPtr motionConverter)
{
    try
    {
        convertedMotion = motionConverter->convertMotion();
    }
    catch(...)
    {
        teptr = std::current_exception();
    }
}

void ConvertingMotionHandlerDialog::updateTimerCallback(void* data, SoSensor* sensor)
{
    ConvertingMotionHandlerDialog* dialog = static_cast<ConvertingMotionHandlerDialog*>(data);
    dialog->update();
}

void ConvertingMotionHandlerDialog::update() {
    if (teptr) {
        updateTimer->unschedule();
        this->close();
    }

    float currentTimestep = motionConverter->getCurrentTimestep();
    ui->ProgressBar->setValue(currentTimestep * scaling);

    float timespan = (SbTime::getTimeOfDay() - startingTime).getValue();
    float percent = (currentTimestep - minTimestep) / (maxTimestep - minTimestep);
    if (percent > 0) {
        float remainingTime = MMM::Math::roundf(timespan * (1.0f / percent) * (1.0f - percent), 0);
        ui->TimeLabel->setText(QString::fromStdString("Remaining time: " + boost::lexical_cast<std::string>(remainingTime) + " seconds"));
    }

    if (convertedMotion) {
        updateTimer->unschedule();
        this->close();
        emit motionConverted(convertedMotion);
    }
}

void ConvertingMotionHandlerDialog::cancel() {
    motionConverter->cancel();
}
