#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>

#include "MMMConverterConfiguration.h"

MMMConverterConfiguration::MMMConverterConfiguration() : ApplicationBaseConfiguration()
{
}

bool MMMConverterConfiguration::processCommandLine(int argc, char *argv[])
{

    VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
    VirtualRobot::RuntimeEnvironment::considerKey("motionName");
    VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");

    VirtualRobot::RuntimeEnvironment::considerKey("converterPlugins");
    VirtualRobot::RuntimeEnvironment::considerKey("converterName");
    VirtualRobot::RuntimeEnvironment::considerKey("converterConfigFile");

    VirtualRobot::RuntimeEnvironment::considerKey("outputModelFile");
    VirtualRobot::RuntimeEnvironment::considerKey("outputModelProcessor");
    VirtualRobot::RuntimeEnvironment::considerKey("outputModelProcessorConfigFile");

    VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");

    VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

    inputMotionPath = getParameter("inputMotion", true, true);
    motionName = getParameter("motionName");
    sensorPluginPaths = getParameters("sensorPlugins");

    converterPluginPaths = getParameters("converterPlugins", std::string(MOTION_CONVERTER_PLUGIN_LIB_DIR));
    converterName = getParameter("converterName");
    if (converterName.empty()) converterName = "NloptConverter";
    converterConfigFilePath = getParameter("converterConfigFile", true, true);

    outputModelFilePath = getParameter("outputModelFile", true, true);
    outputModelProcessor = getParameter("outputModelProcessor");
    outputModelConfigFilePath = getParameter("outputModelProcessorConfigFile", false, true);

    outputMotionPath = getParameter("outputFile");
    if (outputMotionPath.empty()) outputMotionPath = "MMMConverter_output.xml";

    return valid;
}

void MMMConverterConfiguration::print()
{
    MMM_INFO << "*** MMMMotionConverter Configuration ***" << std::endl;
    std::cout << "Input file: " << inputMotionPath << std::endl;
    std::cout << "Converter: " << converterName << std::endl;
    std::cout << "Converter config file: " << converterConfigFilePath << std::endl;
    std::cout << "output model file: " << outputModelFilePath << std::endl;
    std::cout << "output model processor: " << outputModelProcessor << std::endl;
    std::cout << "output model config file: " << outputModelConfigFilePath << std::endl;
    std::cout << "Output file: " << outputMotionPath << std::endl;
}
