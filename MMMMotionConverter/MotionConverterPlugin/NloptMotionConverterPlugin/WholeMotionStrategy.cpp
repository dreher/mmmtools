#include "WholeMotionStrategy.h"

using namespace MMM;

WholeMotionStrategy::WholeMotionStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping) :
    ConvertingStrategy(labeledMarkerData, outputMotion, outputModel, joints, markerMapping),
    dimension(frameDimension * labeledMarkerData.size())
{
}

void WholeMotionStrategy::cancel() {
    MMM_INFO << "WholeMotionStrategy cannot be cancelled!" << std::endl;
}

float WholeMotionStrategy::getCurrentTimestep() {
    return 0.0f;
}

void WholeMotionStrategy::convert() {
    MMM_INFO << "Convert WholeMotion" << std::endl;

    // Build initial configuration for optimization
    std::vector<double> configuration(dimension, 0.0);

    // Initialize optimization
    nlopt::opt optimizer(nloptAlgorithm, dimension);
    optimizer.set_min_objective(ConvertingStrategy::objectiveFunctionWrapperStatic, this);
    optimizer.set_ftol_abs(0.0001);

    setOptimizationBounds(optimizer);

    // Run optimization
    MMM_INFO << "Starting optimization of whole motion (" << labeledMarkerData.size() << " frames)..." << std::endl;
    double objectiveValue;
    try {
        nlopt::result resultCode = optimizer.optimize(configuration, objectiveValue);
        MMM_INFO << "Optimization finished with code " << resultCode << "." << std::endl;
    }
    catch (nlopt::roundoff_limited) {
        MMM_INFO << "Optimization finished by throwing nlopt::roundoff_limited (the result should be usable)." << std::endl;
    }

    Eigen::Vector3f rootPos = Eigen::Vector3f::Zero();
    Eigen::Vector3f rootRot = Eigen::Vector3f::Zero();

    int frameNum = 0;
    for (const auto &labeledMarker : labeledMarkerData) {
        std::vector<double> frameConfiguration(configuration.begin() + frameNum * frameDimension, configuration.begin() + (frameNum + 1) * frameDimension);

        float timestep = labeledMarker.first;

        // calculate ModelPoseSensorMeasurement
        rootPos[0] += frameConfiguration[0]; rootPos[1] += frameConfiguration[1]; rootPos[2] += frameConfiguration[2];
        rootRot[0] += frameConfiguration[3]; rootRot[1] += frameConfiguration[4]; rootRot[2] += frameConfiguration[5];
        if (rootRot[0] > M_PI) rootRot[0] -= 2 * M_PI; if (rootRot[0] < -M_PI) rootRot[0] += 2 * M_PI;
        if (rootRot[1] > M_PI) rootRot[1] -= 2 * M_PI; if (rootRot[1] < -M_PI) rootRot[1] += 2 * M_PI;
        if (rootRot[2] > M_PI) rootRot[2] -= 2 * M_PI; if (rootRot[2] < -M_PI) rootRot[2] += 2 * M_PI;

        ModelPoseSensorMeasurementPtr modelPoseSensorMeasurement(new ModelPoseSensorMeasurement(timestep, rootPos, rootRot));
        outputModelPoseSensor->addSensorMeasurement(modelPoseSensorMeasurement);

        // calculate KinematicSensorMeasurement
        Eigen::VectorXf jointValues(joints.size());
        for (int i = 0; i < jointValues.rows(); ++i) {
            jointValues[i] = frameConfiguration[6 + i];
        }

        KinematicSensorMeasurementPtr kinematicSensorMeasurement(new KinematicSensorMeasurement(timestep, jointValues));
        outputKinematicSensor->addSensorMeasurement(kinematicSensorMeasurement);

        frameNum++;
    }
}

double WholeMotionStrategy::objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad) {
    if (!grad.empty()) {
        MMM_ERROR << "NloptConverter: Gradient computation not supported!" << std::endl;
        return 0.0;
    }

    if (configuration.size() != dimension) {
        MMM_ERROR << "NloptConverter: x has wrong number of dimensions (" << configuration.size() << ")!" << std::endl;
        return 0.0;
    }

    double totalSumDistanceSquares = 0.0;
    std::vector<double> currentPosRot(6, 0.0);

    int frameNum = 0;
    for (const auto &labeledMarker : labeledMarkerData) {

        // Very inefficient
        std::vector<double> frameConfiguration(configuration.begin() + frameNum * frameDimension, configuration.begin() + (frameNum + 1) * frameDimension);
        for (int i = 0; i < 6; ++i) {
            currentPosRot[i] += frameConfiguration[i];
            frameConfiguration[i] = currentPosRot[i];
        }

        setOutputModelConfiguration(frameConfiguration);

        totalSumDistanceSquares += calculateMarkerDistancesSquaresSum(labeledMarker.second);

        frameNum++;
    }

    return totalSumDistanceSquares;
}


void WholeMotionStrategy::setOptimizationBounds(nlopt::opt& optimizer) const {
    // Some algorithms cannot handle unconstraint components (i.e. upper/lower limit of +/- infinity)
    const double frame0PositionLowerBound = -10000.0, frame0PositionUpperBound = 10000.0;  // 10m
    const double positionMaxChange = 100.0;  // 10cm

    const double frame0RotationLowerBound = -M_PI, frame0RotationUpperBound = M_PI;
    const double rotationMaxChange = 0.2;

    std::vector<double> lowerBounds, upperBounds;

    for (unsigned int frame = 0; frame < labeledMarkerData.size(); ++frame) {
        for (int i = 0; i < 2; ++i) {  // translation & rotation vectors
            for (int j = 0; j < 3; ++j) {
                if (frame) {
                    lowerBounds.push_back(i ? -rotationMaxChange : -positionMaxChange);
                    upperBounds.push_back(i ? rotationMaxChange : positionMaxChange);
                } else {
                    lowerBounds.push_back(i ? frame0RotationLowerBound : frame0PositionLowerBound);
                    upperBounds.push_back(i ? frame0RotationUpperBound : frame0PositionUpperBound);
                }
            }
        }

        for (auto jointName : joints) {
            JointInfo jointInfo = outputModel->getModelNode(jointName)->joint;
            lowerBounds.push_back(jointInfo.limitLo);
            upperBounds.push_back(jointInfo.limitHi);
        }
    }

    optimizer.set_lower_bounds(lowerBounds);
    optimizer.set_upper_bounds(upperBounds);
}
