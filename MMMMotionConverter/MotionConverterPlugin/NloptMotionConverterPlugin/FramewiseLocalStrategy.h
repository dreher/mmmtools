/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_FRAMEWISELOCALSTRATEGY_h_
#define _MMM_FRAMEWISELOCALSTRATEGY_h_

#include <MMM/MMMCore.h>

#include "ConvertingStrategy.h"
#include <atomic>

namespace MMM
{

class MMM_IMPORT_EXPORT FramewiseLocalStrategy : public ConvertingStrategy
{
public:
    FramewiseLocalStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping);

    void cancel();

    float getCurrentTimestep();

    void convert();

protected:
    double objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad);

private:
    void calculateMarkerDistancesAverageMaximum(const std::map<std::string, Eigen::Vector3f> &labeledMarker, double &avgDistance, double &maxDistance) const;

    void setOptimizationBounds(nlopt::opt& optimizer) const;

    float currentTimestep;
    bool cancelled;
};

typedef boost::shared_ptr<FramewiseLocalStrategy> FramewiseLocalStrategyPtr;

}

#endif
