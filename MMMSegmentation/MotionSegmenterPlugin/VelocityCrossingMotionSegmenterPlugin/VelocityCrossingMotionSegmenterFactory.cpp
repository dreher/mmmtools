#include "VelocityCrossingMotionSegmenterFactory.h"
#include "VelocityCrossingMotionSegmenter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionSegmenterFactory::SubClassRegistry VelocityCrossingMotionSegmenterFactory::registry(VelocityCrossingMotionSegmenter::NAME, &VelocityCrossingMotionSegmenterFactory::createInstance);

VelocityCrossingMotionSegmenterFactory::VelocityCrossingMotionSegmenterFactory() : MotionSegmenterFactory() {}

VelocityCrossingMotionSegmenterFactory::~VelocityCrossingMotionSegmenterFactory() {}

std::string VelocityCrossingMotionSegmenterFactory::getName()
{
    return VelocityCrossingMotionSegmenter::NAME;
}

MotionSegmenterPtr VelocityCrossingMotionSegmenterFactory::createMotionSegmenter(MotionPtr motion, SegmentationType segType) {
    return MotionSegmenterPtr(new VelocityCrossingMotionSegmenter(motion, segType));
}

MotionSegmenterFactoryPtr VelocityCrossingMotionSegmenterFactory::createInstance(void *)
{
    return MotionSegmenterFactoryPtr(new VelocityCrossingMotionSegmenterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionSegmenterFactoryPtr getFactory() {
    return MotionSegmenterFactoryPtr(new VelocityCrossingMotionSegmenterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionSegmenterFactory::VERSION;
}
