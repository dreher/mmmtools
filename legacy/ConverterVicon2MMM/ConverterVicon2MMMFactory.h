/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_ConverterVicon2MMMFactory_h_
#define _MMM_ConverterVicon2MMMFactory_h_

#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Motion/Legacy/Converter/ConverterFactory.h>
#include <boost/shared_ptr.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>


#include "ConverterVicon2MMMImportExport.h"

namespace MMM
{

/*!
	A factory to create a ConverterVicon2MMM converter.
*/
class ConverterVicon2MMM_IMPORT_EXPORT ConverterVicon2MMMFactory  : public ConverterFactory
{
public:

	ConverterVicon2MMMFactory();
	virtual ~ConverterVicon2MMMFactory();

	ConverterPtr createConverter();

	static std::string getName();

	static boost::shared_ptr<ConverterFactory> createInstance(void*);

private:
	static SubClassRegistry registry;
};

typedef boost::shared_ptr<ConverterVicon2MMMFactory> ConverterVicon2MMMFactoryPtr;

} // namespace MMM

#endif // _MMM_ConverterVicon2MMMFactory_h_
