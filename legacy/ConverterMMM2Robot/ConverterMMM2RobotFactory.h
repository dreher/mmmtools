/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_ConverterMMM2RobotFactory_h_
#define _MMM_ConverterMMM2RobotFactory_h_

#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Motion/Legacy/Converter/ConverterFactory.h>
#include <boost/shared_ptr.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>


#include "ConverterMMM2RobotImportExport.h"

namespace MMM
{

/*!
	A factory to create a ConverterMMM2Robot converter.
*/
class ConverterMMM2Robot_IMPORT_EXPORT ConverterMMM2RobotFactory  : public ConverterFactory
{
public:

	ConverterMMM2RobotFactory();
	virtual ~ConverterMMM2RobotFactory();

	ConverterPtr createConverter();

	static std::string getName();

	static boost::shared_ptr<ConverterFactory> createInstance(void*);

private:
	static SubClassRegistry registry;
};

typedef boost::shared_ptr<ConverterMMM2RobotFactory> ConverterMMM2RobotFactoryPtr;

} // namespace MMM

#endif // _MMM_ConverterMMM2RobotFactory_h_
