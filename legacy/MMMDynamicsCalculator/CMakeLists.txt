cmake_minimum_required(VERSION 2.8.12)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMDynamicsCalculator)

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMDynamicsCalculator.cpp
)

set(HEADER_FILES
    MMMDynamicsCalculatorConfiguration.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
setupSimoxExternalLibraries()
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} ${Simox_EXTERNAL_LIBRARIES})

set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMSimoxTools)

###########################################################
#### Project build configuration                       ####
###########################################################

add_executable(${PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)

set(MMMDYNAMICSCALCULATOR_BASE_DIR ${PROJECT_SOURCE_DIR} CACHE INTERNAL "" )
add_definitions(-DMMMDYNAMICSCALCULATOR_BASE_DIR="${MMMDYNAMICSCALCULATOR_BASE_DIR}" -D_SCL_SECURE_NO_WARNINGS)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    #LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include
    COMPONENT bin
)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

# if the needed C++11 features are known (e.g. range based loops) and cmake >= 3.1.0 is available
# consider using e.g. target_compile_features(foobar PRIVATE cxx_range_for) instead of the "-std=c++11"
# part below. http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake/20165220#20165220

if(MSVC)
    target_compile_options(${PROJECT_NAME} PUBLIC
        "/W4" # enable warnings
        "/MP" # enable parallel build
    )
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    target_compile_options(${PROJECT_NAME} PUBLIC
        "-std=c++11" # enable C++11
        "-Wall" # enable warnings
        "-Wno-long-long"
        "-pedantic"
    )
endif()
