/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ApplicationBaseConfiguration_H_
#define __MMM_ApplicationBaseConfiguration_H_

#include <string>
#include <vector>
#include <set>

class ApplicationBaseConfiguration
{
protected:
    ApplicationBaseConfiguration();

    std::vector<std::string> getParameters(const std::string &parameterName, std::string baseValue);

    std::vector<std::string> getParameters(const std::string &parameterName, std::vector<std::string> baseValues = std::vector<std::string>());

    std::string getParameter(const std::string &parameterName, bool required = false, bool isFilePath = false);

    void print(const std::vector<std::string> &pluginPaths);

    bool valid;
};

#endif
