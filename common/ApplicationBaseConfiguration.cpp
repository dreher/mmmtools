/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>

#include "ApplicationBaseConfiguration.h"

ApplicationBaseConfiguration::ApplicationBaseConfiguration() : valid(true)
{
}

std::vector<std::string> ApplicationBaseConfiguration::getParameters(const std::string &parameterName, std::string baseValue) {
    std::vector<std::string> baseValues;
    baseValues.push_back(baseValue);
    return getParameters(parameterName, baseValue);
}

std::vector<std::string> ApplicationBaseConfiguration::getParameters(const std::string &parameterName, std::vector<std::string> baseValues)
{
    std::vector<std::string> parameters;
    std::string paths = getParameter(parameterName);
    std::string splitStr = ";,";
    boost::split(parameters, paths, boost::is_any_of(splitStr));
    for (const std::string &parameter : parameters) {
        if(!parameter.empty()) baseValues.push_back(parameter);
    }
    return baseValues;
}

std::string ApplicationBaseConfiguration::getParameter(const std::string &parameterName, bool required, bool isFilePath)
{
    if (VirtualRobot::RuntimeEnvironment::hasValue(parameterName))
    {
        std::string value = VirtualRobot::RuntimeEnvironment::getValue(parameterName);

        if (isFilePath && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(value))
        {
            MMM_ERROR << "Invalid path: " << value << std::endl;
            valid = false;
            return std::string();
        }

        return value;
    }
    else if (required)
    {
        MMM_ERROR << "Missing parameter " << parameterName << "!" << std::endl;
        valid = false;
    }

    return std::string();
}

void print(const std::vector<std::string> &pluginPaths) {
    for (auto pluginPath : pluginPaths) {
        std::cout << pluginPath << ";";
    }
    std::cout << std::endl;
}
