/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_C3DMOTIONIMPORTERDIALOG_H_
#define __MMM_C3DMOTIONIMPORTERDIALOG_H_

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <vector>
#include <tuple>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include "../C3DConverter.h"
#include "../../common/MotionListWidget.h"
#endif

namespace Ui {
class C3DMotionImporterDialog;
}

class C3DMotionImporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit C3DMotionImporterDialog(const std::string &motionFilePath, QWidget* parent = 0);
    ~C3DMotionImporterDialog();

    MMM::MotionList getMotions();

private slots:
    void importMotion();

private:
    std::vector<std::tuple<std::string, std::string> > createNamePrefixMap();

    Ui::C3DMotionImporterDialog* ui;
    MotionListWidget* motionList;
    std::vector<std::string> markerPrefixes;
    MMM::C3DConverterPtr converter;
    MMM::MotionList motions;
};

#endif // C3DMotionImporterDIALOG_H
