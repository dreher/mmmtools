#include "C3DMotionImporterFactory.h"
#include "C3DMotionImporter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry C3DMotionImporterFactory::registry(C3DMotionImporter::NAME, &C3DMotionImporterFactory::createInstance);

C3DMotionImporterFactory::C3DMotionImporterFactory() : MotionHandlerFactory() {}

C3DMotionImporterFactory::~C3DMotionImporterFactory() {}

std::string C3DMotionImporterFactory::getName() {
    return C3DMotionImporter::NAME;
}

MotionHandlerPtr C3DMotionImporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new C3DMotionImporter(widget));
}

MotionHandlerFactoryPtr C3DMotionImporterFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new C3DMotionImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new C3DMotionImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
