#include "AddWholeBodyDynamicHandlerFactory.h"
#include "AddWholeBodyDynamicHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddWholeBodyDynamicHandlerFactory::registry(AddWholeBodyDynamicHandler::NAME, &AddWholeBodyDynamicHandlerFactory::createInstance);

AddWholeBodyDynamicHandlerFactory::AddWholeBodyDynamicHandlerFactory() : MotionHandlerFactory() {}

AddWholeBodyDynamicHandlerFactory::~AddWholeBodyDynamicHandlerFactory() {}

std::string AddWholeBodyDynamicHandlerFactory::getName() {
    return AddWholeBodyDynamicHandler::NAME;
}

MotionHandlerPtr AddWholeBodyDynamicHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new AddWholeBodyDynamicHandler(widget));
}

MotionHandlerFactoryPtr AddWholeBodyDynamicHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddWholeBodyDynamicHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddWholeBodyDynamicHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
