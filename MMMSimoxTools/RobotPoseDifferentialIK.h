/**
* This file is part of Simox.
*
* Simox is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* Simox is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 Nikolaus Vahrenkamp
*             GNU Lesser General Public License
*
*/

#ifndef _MMM_RobotPoseDiffIK_h_
#define _MMM_RobotPoseDiffIK_h_

#include <VirtualRobot/VirtualRobotCommon.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include "MMMSimoxToolsImportExport.h"

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

namespace MMM
{


class MMMSimoxTools_IMPORT_EXPORT RobotPoseDifferentialIK : public VirtualRobot::DifferentialIK, public boost::enable_shared_from_this<RobotPoseDifferentialIK>
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	/*!
		@brief Initialize a Jacobian object.
		\param rns The robotNodes (i.e., joints) for which the Jacobians should be calculated.
		\param coordSystem The coordinate system in which the Jacobians are defined. By default the global coordinate system is used.
	*/
    RobotPoseDifferentialIK(VirtualRobot::RobotPtr robot,VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr coordSystem = VirtualRobot::RobotNodePtr(), VirtualRobot::JacobiProvider::InverseJacobiMethod invJacMethod = eSVDDamped);
	

	Eigen::MatrixXf getJacobianMatrix(VirtualRobot::SceneObjectPtr tcp, VirtualRobot::IKSolver::CartesianSelection mode);
	
    Eigen::MatrixXf getJacobianMatrix();

	/*!	@brief Compute a single IK step. 
	 	@param stepSize Controls the amount of error to be reduced in each step: \f$ 0 < \beta \leq 1 \f$
		@return The changes \f$\Delta \theta\f$ in the joint angles.
		\note{Note} This does not affect the joints values of the robot.
	*/
	Eigen::VectorXf computeStep(float stepSize=1.0);


	/*!	@brief Computes the complete inverse kinematics. 
	 	@param stepSize Controls the amount of error to be reduced in each step: \f$ 0 < \beta \leq 1 \f$
	 	@param maxSteps Maximal numbers of steps.
		@param minChange The minimal change in joint angles (euclidean distance in radians)
        @param performMinOneStep If set, at least one step is performed (helps escaping local minima, but may also cause pose jittering)
	 	@note{Note}  Sets the node's joint angles automatically.
	*/
    bool computeSteps(float stepSize, float minChange, int maxSteps, bool performMinOneStep = true);


	//void setGoalSO(const Eigen::Matrix4f &goal, VirtualRobot::SceneObjectPtr tcpSO = VirtualRobot::SceneObjectPtr(), VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::Position, float tolerancePosition = 5.0f, float toleranceRotation = 3.0f / 180.0f*M_PI);
	//Eigen::MatrixXf getPseudoInverseJacobianMatrixSO(VirtualRobot::SceneObjectPtr tcpSO, VirtualRobot::IKSolver::CartesianSelection mode);

	//Eigen::VectorXf getDeltaToGoalSO(VirtualRobot::SceneObjectPtr tcp);

    /*!
        If enabled (standard), joint limits are considered via box constraints.
    */
    void boxConstraints(bool enable);

protected:

    bool checkTolerances();

    Eigen::MatrixXd computePseudoInverseJacobianMatrixDampedD(const Eigen::MatrixXd &m);
    std::vector<Eigen::MatrixXf> localJacobians;
	VirtualRobot::RobotPtr robot;

    bool considerBoxConstraints;

    Eigen::VectorXf _lLimits;
    Eigen::VectorXf _uLimits;
};

typedef boost::shared_ptr<RobotPoseDifferentialIK> RobotPoseDifferentialIKPtr;

} // namespace MMM

#endif // _MMM_RobotPoseDiffIK_h_

