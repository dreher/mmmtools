cmake_minimum_required(VERSION 2.8.12)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/CMakeModules)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMSimoxTools)

set(PROJ_VERSION 0.0.2)
set(PROJ_SO_VERSION 1) # shared lib (.so file) build number

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMSimoxTools.cpp
    RobotPoseDifferentialIK.cpp
)

set(HEADER_FILES
    MMMSimoxTools.h
    RobotPoseDifferentialIK.h
    MMMSimoxToolsImportExport.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
setupSimoxExternalLibraries()
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} ${Simox_EXTERNAL_LIBRARIES})

###########################################################
#### Project build configuration                       ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)

# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..> # TODO: other code requires headers like "#include <MMMSimoxTools/xyz.h>" so we need
    $<INSTALL_INTERFACE:include>                      # TODO: the parent dir for BUILD_INTERFACE. find a more elegant way i guess?
)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    #RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include # seems unnecessary because of target_include_directories()
    COMPONENT lib
)

install(FILES ${HEADER_FILES} DESTINATION "include/${PROJECT_NAME}" COMPONENT dev)

###########################################################
#### Version configuration                             ####
###########################################################

set_property(TARGET ${PROJECT_NAME} PROPERTY VERSION ${PROJ_VERSION})
set_property(TARGET ${PROJECT_NAME} PROPERTY SOVERSION ${PROJ_SO_VERSION})

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
    VERSION ${PROJ_VERSION}
    COMPATIBILITY AnyNewerVersion
)

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

set(CMAKE_POSITION_INDEPENDENT_CODE ON) # enable -fPIC

# if the needed C++11 features are known (e.g. range based loops) and cmake >= 3.1.0 is available
# consider using e.g. target_compile_features(foobar PRIVATE cxx_range_for) instead of the "-std=c++11"
# part below. http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake/20165220#20165220

if(MSVC)
    target_compile_options(${PROJECT_NAME} PUBLIC
        "/W4" # enable warnings
        "/MP" # enable parallel build
    )
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    target_compile_options(${PROJECT_NAME} PUBLIC
        "-std=c++11" # enable C++11
        "-Wall" # enable warnings
        "-Wno-long-long"
        "-pedantic"
    )
endif()
